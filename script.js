
fetch('https://api.trello.com/1/members/me/boards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89', {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(boardArray => Create_Boards(boardArray))
  .catch(err => console.error(err));

function Create_Boards(boardArray){
    boardArray.forEach(element => {
        AddBoard(element);
    });
}

const container=document.querySelector('.board-container');

function AddBoard(element) {
    
    const Board = document.createElement("div");
    Board.classList.add('board');


    Board.style.backgroundColor = element.prefs.backgroundColor;

    if(element.prefs.backgroundImage) {
      const bgUrl = element.prefs.backgroundImageScaled[2].url;
      console.log(bgUrl);
      Board.style.backgroundImage = `url(${bgUrl})`;
      Board.style.backgroundSize='400px 200px';
      Board.style.backgroundRepeat='no-repeat';
    }
    // console.log(Board);
    const Board_name = document.createElement('h2');
    Board_name.classList.add("Board_Name");
    Board_name.innerHTML=element.name;
    const Delete_Button = document.createElement('button');
    Delete_Button.classList.add('btn-outline-danger');
    Delete_Button.innerHTML='Delete';
    Delete_Button.addEventListener("click", event => DeleteBoard(event,element));


    Board.setAttribute('id',element.id);

    Board.insertAdjacentElement("afterbegin",Board_name);
    Board.insertAdjacentElement("beforeend",Delete_Button);
    container.insertAdjacentElement("afterbegin",Board);
}

document.querySelector('.create-board').addEventListener("click", ()=>{
popUp();

})

document.querySelector('.board-container').addEventListener("click",(e)=>{
    if(e.target.classList.contains("board") && !e.target.classList.contains("create-board")){

        location.href = `./list.html?id=${e.target.getAttribute("id")}`;
    }
    })
    



// ************************pop-up function***********************************

const PopUpBody = document.querySelector('body');

function popUp(){
    const PopUp_container=document.createElement('div');
    PopUp_container.classList.add('popContainer');

    const PopUpMenu = document.createElement('div');
    PopUpMenu.classList.add('popupMenu');

    const inputMenu = document.createElement('div');
    inputMenu.classList.add('input-menu');

    const inputBox=document.createElement('input');
    inputBox.classList.add('input-group-text');
    inputBox.setAttribute('placeholder','Enter name of Board');
    const closeBtn= document.createElement('button');
    closeBtn.innerHTML="X";
    closeBtn.addEventListener("click",()=> SetLocation());

    inputMenu.appendChild(inputBox);
    inputMenu.appendChild(closeBtn);

    const popUpButton = document.createElement('button');
    popUpButton.classList.add('btn-success');
    popUpButton.innerHTML="Add Board";
    popUpButton.addEventListener("click", function AddNewBoard(){
        const BoardName=document.querySelector('input').value;
        console.log(BoardName);
        
        fetch(`https://api.trello.com/1/boards/?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${BoardName}`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(element => {
                AddBoard(element)
                SetLocation();
            })
            .catch(err => console.error(err));
    });
    PopUpMenu.appendChild(inputMenu);
    PopUpMenu.appendChild(popUpButton);

    PopUp_container.appendChild(PopUpMenu);

    PopUpBody.appendChild(PopUp_container);
    
}

// ################################################  Set Location   ######################################
function SetLocation(){
    location="index.html";
}

// *******************************************  Delete board ****************************************************

function DeleteBoard(event,element){
const Board_Id=element.id;
fetch(`https://api.trello.com/1/boards/${Board_Id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
    method: 'DELETE'
  })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.json();
    })
    .then(element => {
        event.target.parentNode.remove();
    }) 
    .catch(err => console.error(err));
}
