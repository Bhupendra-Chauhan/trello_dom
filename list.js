let url = new URLSearchParams(window.location.search);
console.log(url);
let boardId = url.get('id');
console.log(boardId);


/* fetching the lists */

fetch(`https://api.trello.com/1/boards/${boardId}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
  method: 'GET',
  headers: {
    'Accept': 'application/json'
  }
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(element =>
    {
        const mainContainer=document.querySelector('.list-container');
        mainContainer.style.backgroundColor=element.prefs.backgroundColor;
        if(element.prefs.backgroundImage) {
            const bgUrl = element.prefs.backgroundImageScaled[5].url;
            console.log(bgUrl);
            mainContainer.style.backgroundImage = `url(${bgUrl})`;
            mainContainer.style.backgroundSize='100% 100%';
            mainContainer.style.backgroundRepeat='no-repeat';
          }
    createList(element.name,element.id)})
  .then(GetList())
  .catch(err => console.error(err));


const createList=(name,id)=>{
    const container=document.querySelector('.BoardName');
    const Boardname=document.createElement('div');
    Boardname.id=id;
    Boardname.innerHTML =`<h1>${name}</h1>`;
    container.appendChild(Boardname);

    

}


/* Get the list for particular board */

function GetList(){

    fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
    method: 'GET',
    headers: {
        'Accept': 'application/json'
    }
    })
    .then(response => {
        console.log(
        `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
    })
    .then(boardArray => fetchLists(boardArray))
    .catch(err => console.error(err));

    function fetchLists(boardArray){
        boardArray.forEach(element => {
            // createList(element.name,element.id);
            
            const container=document.querySelector('.List');
            const list=document.createElement('div');
            list.classList.add('name');
            // list.id=element.id;
            // list.innerHTML =`<h3>${element.name}</h3>`;



            const ListHeader=document.createElement('div');
            ListHeader.className = 'list-header'
            ListHeader.innerHTML+=`<h3>${element.name}</h3>`;
            ListHeader.innerHTML += `<div class="btn btn-outline-danger">Delete</div>`

            // const Delete_Button = document.createElement('button');
            // Delete_Button.classList.add('btn-outline-danger');
            // Delete_Button.innerHTML='Delete';

            ListHeader.lastElementChild.addEventListener("click", event => DeleteList(event,element));

            //list.insertAdjacentElement("beforeend",Delete_Button);
            
            // ListHeader.appendChild(Delete_Button);
            
            list.appendChild(ListHeader)

/* get the cards */
//element: list array
            const card_container = document.createElement('div');
            
                fetch(`https://api.trello.com/1/lists/${element.id}/cards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
                    method: 'GET'
                })
                    .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    );
                    return response.json();
                    })
                    .then(text => {
                        text.forEach(card=>{
                            const cardItem = document.createElement('div');
                            cardItem.className = 'cardContainer';
                            cardItem.id = card.id;
                            cardItem.innerHTML =`
                                <div class="card-name">${card.name}</div>
                                <div class='delete-card'><i class="fa fa-trash-o delete-card" aria-hidden="true"></i></div>
                            `;

                            card_container.appendChild(cardItem);
                        })
                    })
                    .then(()=>{
                        card_container.innerHTML+=`<div class="add-card">+ Add new Card</div>`
                        card_container.addEventListener('click', (e)=>{
                            //console.log(e.target.parentNode.parentNode)
                            if(e.target.classList.contains('add-card')){
                                popUpCard(element.id);
                            }else if(e.target.classList.contains('delete-card')) {
                                let parentNodeEl;
                                if (e.target.tagName === 'DIV') {
                                    //console.log(e.target.id)
                                    parentNodeEl = e.target.parentElement;
                                } else {
                                    parentNodeEl = e.target.parentElement.parentElement;
                                }

                                DeleteCard(parentNodeEl, parentNodeEl.id);
                            } else if(e.target.classList.contains('cardContainer') || e.target.classList.contains("card-name") ){
                                popUpCheckList(e.target.firstElementChild.textContent,e.target);
                             }

                        })
                    })
                    .catch(err => console.error(err));
            list.append(card_container);
            container.insertAdjacentElement("afterbegin",list);
        });
    }
}

//Delete a List

function DeleteList(event,element){
    const Board_Id=element.id;
    fetch(`https://api.trello.com/1/lists/${Board_Id}/closed?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&value=true`, {
        method: 'PUT'
    })
        .then(response => {
        console.log(
            `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
        })
        .then(()=> {
            event.target.parentNode.parentNode.remove();
        }) 
        .catch(err => console.error(err));
}

document.querySelector('.addList').addEventListener("click",()=>{
    popUpList();
})



/* To delete a card */


function DeleteCard(cardItem, id){
    // event.stopPropagation();
    const Card_Id=id;
    console.log(Card_Id);
    fetch(`https://api.trello.com/1/cards/${Card_Id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
        method: 'DELETE'
    })
        .then(response => {
        console.log(
            `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
        })
        .then(element => {
            cardItem.remove();
        }) 
        .catch(err => console.error(err));

}





const PopUpBody = document.querySelector('body');
/* List PopUp */
function popUpList(){
    const PopUp_container=document.createElement('div');
    PopUp_container.classList.add('popContainer');

    const PopUpMenu = document.createElement('div');
    PopUpMenu.classList.add('popupMenu');

    const inputMenu = document.createElement('div');
    inputMenu.classList.add('input-menu');

    const inputBox=document.createElement('input');
    inputBox.classList.add('input-group-text');
    inputBox.setAttribute('placeholder','Enter name of List');
    const closeBtn= document.createElement('button');
    closeBtn.innerHTML="X";
    closeBtn.addEventListener("click",(e)=> /* SetLocation() */ e.target.parentElement.parentElement.parentElement.style.display='none');

    inputMenu.appendChild(inputBox);
    inputMenu.appendChild(closeBtn);

    const popUpButton = document.createElement('button');
    popUpButton.classList.add('btn-success');
    popUpButton.innerHTML="Add List";

    popUpButton.addEventListener("click", function AddNewList(e){
        const ListName=e.target.previousElementSibling.firstElementChild.value;
        console.log(ListName);
        const BoardContainer=document.querySelector('.BoardName');
        const BoardId = BoardContainer.firstChild.id;
        //console.log("BoardId=",BoardId);
        fetch(`https://api.trello.com/1/lists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${ListName}&idBoard=${BoardId}`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(element => {
                //AddList(element)
                SetLocation();
            })
            .catch(err => console.error(err));
    });



    PopUpMenu.appendChild(inputMenu);
    PopUpMenu.appendChild(popUpButton);

    PopUp_container.appendChild(PopUpMenu);

    PopUpBody.appendChild(PopUp_container);
}

/* Card PopUp */
function popUpCard(id){
    const PopUp_container=document.createElement('div');
    PopUp_container.classList.add('popContainer');

    const PopUpMenu = document.createElement('div');
    PopUpMenu.classList.add('popupMenu');

    const inputMenu = document.createElement('div');
    inputMenu.classList.add('input-menu');

    const inputBox=document.createElement('input');
    inputBox.classList.add('input-group-text');
    inputBox.setAttribute('placeholder','Enter name of Card');
    const closeBtn= document.createElement('button');
    closeBtn.innerHTML="X";
    closeBtn.addEventListener("click",(e)=> /* SetLocation() */ e.target.parentElement.parentElement.parentElement.style.display='none');

    inputMenu.appendChild(inputBox);
    inputMenu.appendChild(closeBtn);

    const popUpButton = document.createElement('button');
    popUpButton.classList.add('btn-success');
    popUpButton.innerHTML="Add Card";

    popUpButton.addEventListener("click", function AddNewCard(e){
        // const CardName=document.querySelector('input').value;
        const CardName=e.target.previousElementSibling.firstElementChild.value;
        console.log(CardName);
        // const BoardContainer=document.querySelector('.BoardName');
        // const ListId = BoardContainer.firstChild.id;
        //console.log("BoardId=",BoardId);
        if(CardName!=''){
        fetch(`https://api.trello.com/1/cards?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&idList=${id}&name=${CardName}`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(element => {
                SetLocation();
            })
            .catch(err => console.error(err));
        }
    });


    PopUpMenu.appendChild(inputMenu);
    PopUpMenu.appendChild(popUpButton);

    PopUp_container.appendChild(PopUpMenu);

    PopUpBody.appendChild(PopUp_container);
}


/* PopUp for checklist Container */

function popUpCheckList(card_name,card_id){
    const popUpContainerBackground=document.createElement('div');
    popUpContainerBackground.classList.add('popUpContainerBg');

    const popUpContainer=document.createElement('div') ;
    popUpContainer.classList.add('pop-up-container');

    const popUpHeader= document.createElement('div');
    popUpHeader.classList.add('pop-up-header');

    const CardName=document.createElement('div');
    CardName.classList.add('card-Name');
    CardName.innerHTML=`<h3>${card_name}</h3>`;

    popUpHeader.insertAdjacentElement("afterbegin", CardName);

    const popUpClose = document.createElement('div');
    popUpClose.classList.add('pop-up-close');
    popUpClose.innerHTML = `<div><i class="fa fa-times" aria-hidden="true"></i></div>`;
    popUpClose.addEventListener("click", (e)=>{
        e.target.parentElement.parentElement.parentElement.parentElement.parentElement.style.display='none';
        //SetLocation();
    })

    popUpHeader.insertAdjacentElement("beforeend", popUpClose);

    popUpContainer.insertAdjacentElement("afterbegin", popUpHeader);

    const CheckList_Container = document.createElement('div');
    CheckList_Container.classList.add('checklist-container');

    popUpContainer.insertAdjacentElement("beforeend",CheckList_Container);

    const CheckListDisplay_Container =document.createElement('div');
    CheckListDisplay_Container.classList.add('checklist-display-container');

    displayCheckList(card_id,CheckListDisplay_Container);

    CheckList_Container.insertAdjacentElement("afterbegin", CheckListDisplay_Container);

    const AddCheckList_Btn = document.createElement('p');
    AddCheckList_Btn.classList.add('add-checkList');
    AddCheckList_Btn.innerHTML='+ Add CheckList';

    AddCheckList_Btn.addEventListener("click",(e)=>{
        createCheckListPopUp(card_id);
    })

    CheckList_Container.insertAdjacentElement("beforeend", AddCheckList_Btn);

    popUpContainerBackground.appendChild(popUpContainer);
    PopUpBody.appendChild(popUpContainerBackground);


}


//Checklist popUp to add new checklist

function createCheckListPopUp(card_id){
    const PopUp_container=document.createElement('div');
    PopUp_container.classList.add('popContainer');

    const PopUpMenu = document.createElement('div');
    PopUpMenu.classList.add('popupMenu');

    const inputMenu = document.createElement('div');
    inputMenu.classList.add('input-menu');

    const inputBox=document.createElement('input');
    inputBox.classList.add('input-group-text');
    inputBox.setAttribute('placeholder','Enter name of CheckList');
    const closeBtn= document.createElement('button');
    closeBtn.innerHTML="X";
    closeBtn.addEventListener("click",(e)=>{
        //console.log(e.target.parentElement.parentElement.parentElement);
        e.target.parentElement.parentElement.parentElement.style.display='none';
    });

    inputMenu.appendChild(inputBox);
    inputMenu.appendChild(closeBtn);

    const popUpButton = document.createElement('button');
    popUpButton.classList.add('btn-success');
    popUpButton.innerHTML="Add CheckList";

    popUpButton.addEventListener("click", function AddNewCheckList(e){
        // console.log(e.target.parentElement.parentElement);
        const CheckListName = e.target.previousElementSibling.firstElementChild.value;
        // console.log(CheckListName);

        if(CheckListName===''){
            console.log("Enter the name of checklist");
        }else{

        fetch(`https://api.trello.com/1/cards/${card_id.id}/checklists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${CheckListName}`, {
            method: 'POST'
        })
            .then(response => {
                document.querySelector('input').value='';
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(element => {
                SetLocation();
            })
            .catch(err => console.error(err));
        }
    });



    PopUpMenu.appendChild(inputMenu);
    PopUpMenu.appendChild(popUpButton);

    PopUp_container.appendChild(PopUpMenu);

    PopUpBody.appendChild(PopUp_container);
    
}

// display checklist in checklist container

function displayCheckList(card_id,CheckListDisplay_Container){
    //console.log("printed");
    //console.log(card_id);
        fetch(`https://api.trello.com/1/cards/${card_id.id}/checklists?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
            method: 'GET'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(text => {
                text.forEach((checkList_info)=>
                GetCheckList(checkList_info,CheckListDisplay_Container));
            })
            .catch(err => console.error(err));
}


function SetLocation(){
    location.reload();
}


// function to Get the checklist on container from trello


function GetCheckList(checkList_info, CheckListDisplay_Container){

    //console.log(checkList_info); 
    const checkList_Container=document.createElement('div') ;
    checkList_Container.classList.add('checkList-container');
    checkList_Container.setAttribute("id", checkList_info.id);

    const checkList_Header= document.createElement('div');
    checkList_Header.classList.add('checkList-header');

    const checkListName=document.createElement('div');
    checkListName.classList.add('checkList-Name');
    checkListName.innerHTML=`<h3>${checkList_info.name}</h3>`;

    checkList_Header.insertAdjacentElement("afterbegin", checkListName);

    const DeleteCheckListBtn = document.createElement('div');
    DeleteCheckListBtn.classList.add('delete-checklist-btn');
    DeleteCheckListBtn.innerHTML = `<div><i class="fa fa-trash-o delete-card" aria-hidden="true"></div>`;
    DeleteCheckListBtn.addEventListener("click",(e)=>{
    
        DeleteCheckList(e.target.parentNode.parentNode.parentNode.parentNode);
    })

    checkList_Header.insertAdjacentElement("beforeend", DeleteCheckListBtn);

    checkList_Container.insertAdjacentElement("afterbegin", checkList_Header);

    const CheckList_Item_Container = document.createElement('div');
    CheckList_Item_Container.classList.add('checklist-item-container');

    GetCheckListItem(checkList_info,CheckList_Item_Container);


    checkList_Container.insertAdjacentElement("beforeend",CheckList_Item_Container);

    const AddCheckList_Item_Btn = document.createElement('p');
    AddCheckList_Item_Btn.classList.add('add-checkList-item');
    AddCheckList_Item_Btn.innerHTML='+ Add CheckList Item';

    AddCheckList_Item_Btn.addEventListener("click",(e)=>{
        console.log(e.target);
        createCheckListItemPopUp(checkList_info.id,CheckList_Item_Container);
    })

    checkList_Container.insertAdjacentElement("beforeend",AddCheckList_Item_Btn);

    CheckListDisplay_Container.appendChild(checkList_Container);
}

//function to get the checklist item from tello


function GetCheckListItem(checkList_info,CheckList_Item_Container){
    fetch(`https://api.trello.com/1/checklists/${checkList_info.id}/checkItems?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
        method: 'GET'
        })
        .then(response => {
            console.log(
            `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(text => {
            text.forEach((checkListItem_info)=>{
                const checkList_Item=document.createElement('div');
                checkList_Item.classList.add('checkList-Item');
                checkList_Item.setAttribute("id",checkListItem_info.id);


                const checkBox=document.createElement('input');
                checkBox.classList.add('checkListeItem-checkbox');
                checkBox.setAttribute("type",'checkbox');

                const NameofCheckListItem = document.createElement('h3');
                NameofCheckListItem.classList.add('checkListItem-name')                ;
                NameofCheckListItem.innerHTML=checkListItem_info.name;

                const DeleteCheckListItemBtn = document.createElement('div');
                DeleteCheckListItemBtn.classList.add('delete-checklistItem-btn');
                DeleteCheckListItemBtn.innerHTML = `<div><i class="fa fa-trash-o delete-card delete-checklistItem-btn" aria-hidden="true"></div>`;

                DeleteCheckListItemBtn.addEventListener("click",(e)=>{

                    const checklistID = e.target.parentNode.parentNode.parentNode.parentNode.parentNode.id;
                    console.log(checklistID);
                    console.log(checkListItem_info.id);
    
                    fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems/${checkListItem_info.id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
                            method: 'DELETE'
                        })
                            .then(response => {
                            console.log(
                                `Response: ${response.status} ${response.statusText}`
                            );
                            return response.json();
                            })
                            .then(element => {
                                checkList_Item.remove();
                            }) 
                            .catch(err => console.error(err));

                })

                checkList_Item.appendChild(checkBox);
                checkList_Item.appendChild(NameofCheckListItem);
                checkList_Item.appendChild(DeleteCheckListItemBtn);

                CheckList_Item_Container.appendChild(checkList_Item);

            })
        })
        .catch(err => console.error(err));
}


//pop to add new chceklist item

function createCheckListItemPopUp(checkList_info_id,CheckList_Item_Container){
    const PopUp_container=document.createElement('div');
    PopUp_container.classList.add('popContainer');

    const PopUpMenu = document.createElement('div');
    PopUpMenu.classList.add('popupMenu');

    const inputMenu = document.createElement('div');
    inputMenu.classList.add('input-menu');

    const inputBox=document.createElement('input');
    inputBox.classList.add('input-group-text');
    inputBox.setAttribute('placeholder','Enter name of CheckList Item');
    const closeBtn= document.createElement('button');
    closeBtn.innerHTML="X";

    closeBtn.addEventListener("click",(e)=>{
        //console.log(e.target.parentElement.parentElement.parentElement);
        e.target.parentElement.parentElement.parentElement.style.display='none';
    });
    
    inputMenu.appendChild(inputBox);
    inputMenu.appendChild(closeBtn);

    const popUpButton = document.createElement('button');
    popUpButton.classList.add('btn-success');
    popUpButton.innerHTML="Add CheckListItem";

    popUpButton.addEventListener("click", function AddNewCheckList(e){
        console.log(e.target.parentElement.parentElement);
        const CheckListItemName=e.target.previousElementSibling.firstElementChild.value;

        if(CheckListItemName===''){
            console.log("Enter the name of checklist");
        }else{
            //console.log(CheckListItemName);
        fetch(`https://api.trello.com/1/checklists/${checkList_info_id}/checkItems?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89&name=${CheckListItemName}`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(element => {
                SetLocation();
                
            })
            .catch(err => console.error(err));
        }
    });


    PopUpMenu.appendChild(inputMenu);
    PopUpMenu.appendChild(popUpButton);

    PopUp_container.appendChild(PopUpMenu);

    PopUpBody.appendChild(PopUp_container);
}



//Delete a checklist from container


function DeleteCheckList(checkList_id){
    console.log(checkList_id.id)
    fetch(`https://api.trello.com/1/checklists/${checkList_id.id}?key=8b017ba8f10600ab010f69670ca7858f&token=9b905dd67ccaa0d51e7b588f2f6b7f37b2c284b49a883c783a9731f489623e89`, {
        method: 'DELETE'
    })
        .then(response => {
        console.log(
            `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
        })
        .then(element => {
            checkList_id.remove();
        }) 
        .catch(err => console.error(err));
}






 
